from django.test import TestCase
from .forms import SignUpForm
from projects.models import ProjectCategory
from unittest import skip

DEFAULT_DATA = {
    'username': 'testuser',
    'first_name': 'joe',
    'last_name': 'travolta',
    'company': 'company',
    'email': 'test@test.com',
    'email_confirmation': 'test@test.com',
    'password1': 'testingming',
    'password2': 'testingming',
    'phone_number': '11111111',
    'country': 'Norway',
    'city': 'Trondheim',
    'state': 'Trondheim',
    'postal_code': '1111',
    'street_address': 'Trollvegen 12',
    'categories': ProjectCategory.objects.all()
    }

CATEGORIES = ['Cleaning', 'Painting', 'Gardening']
    

class FormTest(TestCase):
    def setUp(self):
        for i in range(3):
            ProjectCategory.objects.create(name=CATEGORIES[i])

    def test_form_with_default_data(self):
        form = SignUpForm(data=DEFAULT_DATA)
        self.assertTrue(form.is_valid())

    def test_form_too_long_username(self):
        form_data = DEFAULT_DATA.copy()
        form_data['username'] = "joejoejoesjoejoejoesjoejoejoesjoejoejoesjoejoejoesjoejoejoesjoejoejoesjoejoejoesjoejoejoesjoejoejoesjoejoejoesjoejoejoesjoejoejoesjoejoejoesjoejoejoess"
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_form_too_short_username(self):
        form_data = DEFAULT_DATA.copy()
        form_data['username'] = ""
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_form_password_cannot_match_username(self):
        form_data = DEFAULT_DATA.copy()
        test_user = "joejoe"
        form_data['username'] = test_user
        form_data['password1'] = test_user
        form_data['password2'] = test_user
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_form_first_name_cannot_be_empty(self):
        form_data = DEFAULT_DATA.copy()
        form_data['first_name'] = ""
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_form_last_name_cannot_be_empty(self):
        form_data = DEFAULT_DATA.copy()
        form_data['last_name'] = ""
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid())
        
    def test_email_cannot_be_empty(self):
        form_data = DEFAULT_DATA.copy()
        form_data['email'] = ""
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_email_confirmation_cannot_be_empty(self):
        form_data = DEFAULT_DATA.copy()
        form_data['email_confirmation'] = ""
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid())

    @skip('This is a bug!')
    def test_mail_does_not_match(self):
        form_data = DEFAULT_DATA.copy()
        form_data['email_confirmation'] = "t@t.com"
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_form_without_data(self):
        form = SignUpForm(data={})
        self.assertFalse(form.is_valid())

    def test_select_one_project(self):
        form_data = DEFAULT_DATA.copy()
        form_data['categories'] = ProjectCategory.objects.filter(name="Cleaning")
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_select_none_projects(self):
        form_data = DEFAULT_DATA.copy()
        form_data['categories'] = ""
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid())

TEST_NAME = 'Haraldson'
OTHER_NAME = 'Sepp'

TEST_MATRIX_PWD_NAME = [
#              username,  last_name,  first_name, result
            [ TEST_NAME,  TEST_NAME,  OTHER_NAME, False ],
            [ TEST_NAME,  OTHER_NAME, TEST_NAME,  False ],
            [ OTHER_NAME, TEST_NAME,  TEST_NAME,  False ],
            [ OTHER_NAME, OTHER_NAME, OTHER_NAME, True ],
            ]

VALID_PWD = 'ya_qu+dDSa'
INVALID_PWD = "admin"

MAIL1 = 'test@test.com'
MAIL2 = 't@t.com'
INVALID_MAIL = 'abc'

TEST_MATRIX_EMAIL = [
# HEADER:    email, cemail, result
            [MAIL1,        MAIL1,        True],
            [MAIL1,        MAIL2,        False],
            [MAIL1,        INVALID_MAIL, False],
            [INVALID_MAIL, MAIL1,        False],
            [INVALID_MAIL, MAIL2,        False],
            [INVALID_MAIL, INVALID_MAIL, False],
]

class TwoWayDomainTest(TestCase):

    def setUp(self):
        for i in range(3):
            ProjectCategory.objects.create(name=CATEGORIES[i])

    def test_pwd_against_names(self):
        for line in TEST_MATRIX_PWD_NAME:
            form_data = DEFAULT_DATA.copy()
            form_data['username'] = line[0]
            form_data['last_name'] = line[1]
            form_data['first_name'] = line[2]
            form_data['password1'] = TEST_NAME
            form_data['password2'] = TEST_NAME
            form = SignUpForm(data=form_data)
            form.is_valid()
            self.assertEqual(form.is_valid(), line[3])

    def test_pwd1_against_pwd2(self):
        pwd_combinations = [VALID_PWD, INVALID_PWD]
        for pwd1 in pwd_combinations:
            for pwd2 in pwd_combinations:
                form_data = DEFAULT_DATA.copy()
                form_data['password1'] = pwd1
                form_data['password2'] = pwd2
                form = SignUpForm(data=form_data)
                form.is_valid()

                if(pwd1 == pwd2 and pwd1 == VALID_PWD):
                    self.assertTrue(form.is_valid())
                else:
                    self.assertFalse(form.is_valid())

    @skip('This is the email bug!')
    def test_email_matrix(self):
        for line in TEST_MATRIX_EMAIL:
            form_data = DEFAULT_DATA.copy()
            form_data['email'] = line[0]
            form_data['email_confirmation'] = line[1]
            form = SignUpForm(data=form_data)
            form.is_valid()
            self.assertEqual(form.is_valid(), line[2])
