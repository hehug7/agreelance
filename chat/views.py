from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Message

@login_required
def chat(request):
    return render(request, 'chat/chat_index.html', {})

@login_required
def room(request, room_name):
    messages = list(Message.objects.filter(room_name = room_name).order_by("timestamp"))
    msgs = []
    for msg in messages:
        msgs.append("[" + str(msg.formatted_timestamp) + "] " + str(msg.message))

    return render(request, 'chat/room.html', {
        'room_name': room_name,
        'msgs': msgs
    })


@login_required
def room_msg(request, room_name, message):
    new_message, created = Message.objects.get_or_create(
        room_name = room_name,
        message = message)
    new_message.save()

    messages = list(Message.objects.filter(room_name = room_name).order_by("timestamp"))
    msgs = []
    for msg in messages:
        msgs.append(str(msg.message) + " [" + str(msg.formatted_timestamp) + "]")

    return render(request, 'chat/room.html', {
        'room_name': room_name,
        'msgs': msgs
    })