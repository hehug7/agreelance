from django.test import TestCase
from .models import Message
from django.test import Client
from django.contrib.auth.models import User

class ChatTest(TestCase):
    def setUp(self):
        self.c = Client()
        response = self.c.post('/user/login/', {'username': 'admin', 'password': 'qwerty123'})
        self.assertEquals(200, response.status_code)
        self.joe = Client()
        offer = self.joe.post('/user/login/', {'username': 'joe', 'password': 'qwerty123'})
        self.assertEquals(200, response.status_code)

    def test_chat_index(self):
        response = self.c.post('/chat/')
        self.assertEquals(302, response.status_code)

    def test_create_chat_room(self):
        response = self.c.get('/chat/testlobby/')
        self.assertEquals(302, response.status_code)

    def test_message_should_not_exist(self):
        response = self.c.get('/chat/testlobby/hi/')
        self.assertEquals(404, response.status_code)
