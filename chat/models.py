from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

class Message(models.Model):
    room_name = models.CharField(max_length=140)
    message = models.TextField()
    timestamp = models.DateTimeField(default=timezone.now, db_index=True, unique=True)

    @property
    def formatted_timestamp(self):
        return self.timestamp.strftime('%b %d %I:%M %p')
    
    def as_dict(self):
        return {'room_name': self.room_name, 'message': self.message, 'timestamp': self.formatted_timestamp}