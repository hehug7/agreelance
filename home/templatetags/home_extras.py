from django import template
from projects.models import Project, Task, TaskOffer

register = template.Library()

TASK_STATUS_MAPPING = {
    Task.AWAITING_DELIVERY: 'awaiting_delivery',
    Task.PENDING_ACCEPTANCE: 'pending_acceptance',
    Task.PENDING_PAYMENT: 'pending_payment',
    Task.PAYMENT_SENT: 'payment_sent',
    Task.DECLINED_DELIVERY: 'declined_delivery'
}       

@register.filter
def check_nr_pending_offers(project):
    pending_offers = 0
    tasks = project.tasks.all()
    for task in tasks:
        taskoffers = task.taskoffer_set.all()
        for taskoffer in taskoffers:
            if taskoffer.status == TaskOffer.PENDING:
                pending_offers+=1
    #print(pending_offers)
    return pending_offers


@register.filter
def check_nr_user_offers(project, user):
    offers = {}
    pending_offers = 0
    declined_offers = 0
    accepted_offers = 0
    tasks = project.tasks.all()
    for task in tasks:
        taskoffers = task.taskoffer_set.filter(offerer=user.profile)
        for taskoffer in taskoffers:
            if taskoffer.status == TaskOffer.PENDING:
                pending_offers+=1
            elif taskoffer.status == TaskOffer.ACCEPTED:
                accepted_offers+=1
            elif taskoffer.status == TaskOffer.DECLINED:
                declined_offers+=1

    offers['declined'] = declined_offers
    offers['pending'] = pending_offers
    offers['accepted'] = accepted_offers
    #print(offers)
    return offers

@register.filter
def task_status(task):
    delivery_message = "You are awaiting delivery"

    if task.status == Task.PENDING_ACCEPTANCE:
        delivery_message = "You have deliveries waiting for acceptance"
    elif task.status == Task.PENDING_PAYMENT:
        delivery_message = "You have deliveries waiting for payment"
    elif task.status == Task.PAYMENT_SENT:
        delivery_message = "You have sent payment"

    return delivery_message


@register.filter
def get_task_statuses(project):
    task_statuses = {}

    for status in TASK_STATUS_MAPPING:
        task_statuses[TASK_STATUS_MAPPING[status]] = 0

    tasks = project.tasks.all()

    for task in tasks:
        task_statuses[TASK_STATUS_MAPPING[task.status]] += 1

    return task_statuses

@register.filter
def all_tasks(project):
    return project.tasks.all()

@register.filter
def offers(task):
    task_offers = task.taskoffer_set.all()
    print(task_offers)
    msg = "No offers"
    if len(task_offers) > 0:
        x = 0
        msg = "You have "
        for t in task_offers:
            x+=1
            if t.status == 'a':
                return "You have accepted an offer for this task"
        msg += str(x) + " pending offers"
    return msg

@register.filter
def get_user_task_statuses(project, user):
    task_statuses = {}

    for status in TASK_STATUS_MAPPING:
        task_statuses[TASK_STATUS_MAPPING[status]] = 0

    tasks = project.tasks.all()

    for task in tasks:
        try:
            task_offer = task.taskoffer_set.get(status='a')
            if task_offer.offerer == user.profile:
                task_statuses[TASK_STATUS_MAPPING[task.status]] += 1

        except TaskOffer.DoesNotExist:
            pass

    return task_statuses
