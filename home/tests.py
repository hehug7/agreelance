from django.test import TestCase, Client

from .templatetags import home_extras
from projects.models import Task, Project

from django.contrib.auth.models import User

class TemplateTagsHomeExtrasTest(TestCase):

    fixtures = ['test_seed.json']

    def setUp(self):
        self.proj = Project.objects.get(pk=1)

        self.task = Task.objects.create(
            title = "test task 1",
            description = "test task description 1",
            budget = 100,
            project = self.proj)

        self.task2 = Task.objects.create(
            title = "test task 2",
            description = "test task description 2",
            budget = 300,
            project = self.proj)

        self.user = User.objects.get(pk=3)

    def test_task_status(self):
        self.assertEqual(home_extras.task_status(self.task), "You are awaiting delivery")
        self.task.status = Task.PENDING_ACCEPTANCE
        self.assertEqual(home_extras.task_status(self.task), "You have deliveries waiting for acceptance")
        self.task.status = Task.PENDING_PAYMENT
        self.assertEqual(home_extras.task_status(self.task), "You have deliveries waiting for payment")
        self.task.status = Task.PAYMENT_SENT
        self.assertEqual(home_extras.task_status(self.task), "You have sent payment")

    def test_get_task_status(self):
        self.task.status = Task.PENDING_ACCEPTANCE
        self.task2.status = Task.PAYMENT_SENT

        self.task.save()
        self.task2.save()

        statuses = home_extras.get_task_statuses(self.proj)

        self.assertEqual(statuses['awaiting_delivery'], 1)
        self.assertEqual(statuses['pending_acceptance'], 1)
        self.assertEqual(statuses['pending_payment'], 0)
        self.assertEqual(statuses['payment_sent'], 1)
        self.assertEqual(statuses['declined_delivery'], 0)

    def test_get_user_task_status(self):
        self.task.status = Task.PENDING_ACCEPTANCE
        self.task2.status = Task.PAYMENT_SENT
        self.task.save()
        self.task2.save()

        statuses = home_extras.get_user_task_statuses(self.proj, self.user)

        self.assertEqual(statuses['awaiting_delivery'], 1)
        self.assertEqual(statuses['pending_acceptance'], 0)
        self.assertEqual(statuses['pending_payment'], 0)
        self.assertEqual(statuses['payment_sent'], 0)
        self.assertEqual(statuses['declined_delivery'], 0)

class HomeRequestTest(TestCase):

    fixtures = ['test_seed.json']

    def setUp(self):
        self.c = Client()

    def test_authentication(self):
        
        response = self.c.post('', {})

        self.assertEquals(302, response.status_code)
        self.assertEquals('/projects/', response.url)
