from django.http import HttpResponse, HttpResponseRedirect
from user.models import Profile
from .models import Project, Task, TaskFile, TaskOffer, Delivery, ProjectCategory, Team, TaskFileTeam, directory_path
from .forms import ProjectForm, TaskFileForm, ProjectStatusForm, TaskOfferForm, TaskOfferResponseForm, TaskPermissionForm, DeliveryForm, TaskDeliveryResponseForm, TeamForm, TeamAddForm, EditProjectForm, TaskForm

from django.core import mail
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.utils import timezone

def projects(request):
    projects = Project.objects.all()
    project_categories = ProjectCategory.objects.all()
    return render(request,
        'projects/projects.html',
        {
            'projects': projects,
            'project_categories': project_categories,
        }
    )

@login_required
def new_project(request):
    current_site = get_current_site(request)
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.user = request.user.profile
            project.category =  get_object_or_404(ProjectCategory, id=request.POST.get('category_id'))
            project.save()

            people = Profile.objects.filter(categories__id=project.category.id)
            for person in people:
                send_new_project_email(person, project)

            task_title = request.POST.getlist('task_title')
            task_description = request.POST.getlist('task_description')
            task_budget = request.POST.getlist('task_budget')

            for i, title in enumerate(task_title):
                Task.objects.create(
                    title = title,
                    description = task_description[i],
                    budget = task_budget[i],
                    project = project,
                )
            return redirect('project_view', project_id=project.id)
    
    form = ProjectForm()
    return render(request, 'projects/new_project.html', {'form': form})

def send_new_project_email(person, project):
    if person.user.email:
        try:
            with mail.get_connection() as connection:
                mail.EmailMessage(
                    "New Project: " + project.title , 
                    "A new project you might be interested in was created and can be viwed at " + current_site.domain + 
                    '/projects/' + str(project.id), "Agreelancer", [person.user.email],
                    connection=connection,
                ).send()
        except Exception as e:
            messages.success(request, 'Sending of email to ' + person.user.email + " failed: " + str(e))

def project_view(request, project_id):
    project = Project.objects.get(pk=project_id)
    tasks = project.tasks.all()
    total_budget = 0

    if isProjectOwner(request.user, project):
        if isPostRequest(request, 'offer_response'):
            handleOfferResponseRequest(request)

        if isPostRequest(request, 'edit_project'):
            handleEditProjectRequest(request, project)

        if isPostRequest(request, 'add_task'):
            handleAddTaskRequest(request, project)
            tasks = project.tasks.all() 

        if isPostRequest(request, 'status_change'):
            handleStatusChangeRequest(request, project)
        
        for item in tasks:
            total_budget += item.budget

        offer_response_form = TaskOfferResponseForm()
        edit_project_form = EditProjectForm(initial={'description': project.description})
        add_task_form = TaskForm()
        status_form = ProjectStatusForm(initial={'status': project.status})

        return render(request, 'projects/project_view.html', {
            'project': project,
            'tasks': tasks,
            'status_form': status_form,
            'total_budget': total_budget,
            'offer_response_form': offer_response_form,
            'edit_project_form': edit_project_form,
            'add_task_form': add_task_form,
        })

    else:
        if isPostRequest(request, 'offer_submit'):
            handleOfferTransmitRequest(request)
            
        for item in tasks:
            total_budget += item.budget

        task_offer_form = TaskOfferForm()

        return render(request, 'projects/project_view.html', {
        'project': project,
        'tasks': tasks,
        'task_offer_form': task_offer_form,
        'total_budget': total_budget,
        })


def isProjectOwner(user, project):
    return user == project.user.user

def isPostRequest(request, name):
    return request.method == 'POST' and name in request.POST

def handleOfferResponseRequest(request):
    instance = get_object_or_404(TaskOffer, id=request.POST.get('taskofferid'))
    offer_response_form = TaskOfferResponseForm(request.POST, instance=instance)
    if offer_response_form.is_valid():
        offer_response = offer_response_form.save(commit=False)

        if offer_response.status == 'a':
            offer_response.task.read.add(offer_response.offerer)
            offer_response.task.write.add(offer_response.offerer)
            project = offer_response.task.project
            project.participants.add(offer_response.offerer)

        offer_response.save()

def handleEditProjectRequest(request, project):
    edit_form = EditProjectForm(request.POST)
    if edit_form.is_valid():
        project_description = edit_form.save(commit=False)
        project.description = project_description.description
        project.save()

def handleAddTaskRequest(request, project):
    task_form = TaskForm(request.POST)
    if task_form.is_valid():
        task = Task.objects.create(
            title = task_form.cleaned_data['title'],
            description = task_form.cleaned_data['description'],
            budget = task_form.cleaned_data['budget'],
            project = project,
        )

def handleStatusChangeRequest(request, project):
    status_form = ProjectStatusForm(request.POST)
    if status_form.is_valid():
        project_status = status_form.save(commit=False)
        project.status = project_status.status
        project.save()

def handleOfferTransmitRequest(request):
    task_offer_form = TaskOfferForm(request.POST)
    if task_offer_form.is_valid():
        task_offer = task_offer_form.save(commit=False)
        task_offer.task =  Task.objects.get(pk=request.POST.get('taskvalue'))
        task_offer.offerer = request.user.profile
        task_offer.save()

@login_required
def upload_file_to_task(request, project_id, task_id):
    project = Project.objects.get(pk=project_id)
    task = Task.objects.get(pk=task_id)
    user_permissions = get_user_task_permissions(request.user, task)
    accepted_task_offer = task.accepted_task_offer()

    if user_permissions.modify or user_permissions.write or user_permissions.upload:
        if request.method == 'POST':
            if handle_upload_file_to_task_request(request, task):
                return redirect('task_view', project_id=project_id, task_id=task_id)
             
        return render(
            request,
            'projects/upload_file_to_task.html',
            {
                'project': project,
                'task': task,
                'task_file_form': TaskFileForm(),
            }
        )
    return redirect('/user/login')


def handle_upload_file_to_task_request(request, task, user_permissions):
    task_file_form = TaskFileForm(request.POST, request.FILES)
    if not task_file_form.is_valid():
        return False

    task_file = task_file_form.save(commit=False)
    task_file.task = task
    existing_file = task.files.filter(file=directory_path(task_file, task_file.file.file)).first()
    access = user_permissions.modify or user_permissions.owner

    for team in request.user.profile.teams.all():
        file_modify_access  = TaskFileTeam.objects.filter(team=team, file=existing_file, modify=True).exists()
        print(file_modify_access)
        access = access or file_modify_access
    access = access or user_permissions.modify
    if access:
        if existing_file:
            existing_file.delete()
        task_file.save()

        if request.user.profile != project.user and request.user.profile != accepted_task_offer.offerer:
            teams = request.user.profile.teams.filter(task__id=task.id)
            for team in teams:
                tft = TaskFileTeam()
                tft.team = team
                tft.file = task_file
                tft.read = True
                tft.save()
    else:
        messages.warning(request, "You do not have access to modify this file")

    return True

class UserTaskPermissions(object):
    
    def __init__(self):
        self.write = False
        self.read = False
        self.modify = False
        self.owner = False
        self.upload = False
        self.view_task = False

    def to_dict(self):
        return {
            'write': self.write,
            'read': self.read,
            'modify': self.modify,
            'owner': self.owner,
            'upload': self.upload,
            'view_task': self.view_task
        }


def get_user_task_permissions(user, task):

    user_permissions = UserTaskPermissions()

    if isProjectOwner(user, task.project):
        user_permissions.write = True
        user_permissions.read = True
        user_permissions.modify = True
        user_permissions.owner = True
        user_permissions.upload = True
    
    elif task.accepted_task_offer() and task.accepted_task_offer().offerer == user.profile:
        user_permissions.write = True
        user_permissions.read = True
        user_permissions.modify = True
        user_permissions.owner = False
        user_permissions.upload = True

    else:
        user_permissions.read = user.profile.task_participants_read.filter(id=task.id).exists()
        # Team members can view its teams tasks
        user_permissions.upload = user.profile.teams.filter(task__id=task.id, write=True).exists()
        user_permissions.view_task = user.profile.teams.filter(task__id=task.id).exists()
        user_permissions.write = user.profile.task_participants_write.filter(id=task.id).exists()
        user_permissions.modify = user.profile.task_participants_modify.filter(id=task.id).exists()

    return user_permissions

@login_required
def task_view(request, project_id, task_id):
    user = request.user
    project = Project.objects.get(pk=project_id)
    task = Task.objects.get(pk=task_id)
    accepted_task_offer = task.accepted_task_offer()

    user_permissions = get_user_task_permissions(request.user, task)
    if not user_permissions.read and not user_permissions.write and not user_permissions.modify and not user_permissions.owner and not user_permissions.view_task:
        return redirect('/user/login')


    if isPostRequest(request, 'delivery') and is_accepted_task_offer_offerer(accepted_task_offer, user):
        handle_delivery_request(request, task)

    if isPostRequest(request, 'delivery-response'):
        handle_delivery_response_request(request, task)

    if isPostRequest(request, 'team') and is_accepted_task_offer_offerer(accepted_task_offer, user):
        handle_team_request(request, task)

    if isPostRequest(request, 'team-add') and is_accepted_task_offer_offerer(accepted_task_offer, user):
        handle_team_add_request(request)

    if isPostRequest(request, 'permissions') and is_accepted_task_offer_offerer(accepted_task_offer, user):
        handle_permissions_request(request, task)

    deliver_form = DeliveryForm()
    deliver_response_form = TaskDeliveryResponseForm()
    team_form = TeamForm()
    team_add_form = TeamAddForm()

    deliveries = task.delivery.all()
    team_files = []
    teams = user.profile.teams.filter(task__id=task.id).all()
    per = {}
    for f in task.files.all():
        per[f.name()] = {}
        for p in f.teams.all():
            per[f.name()][p.team.name] = p
            if p.read:
                team_files.append(p)

    return render(request, 'projects/task_view.html', {
            'task': task,
            'project': project,
            'user_permissions': user_permissions.to_dict(),
            'deliver_form': deliver_form,
            'deliveries': deliveries,
            'deliver_response_form': deliver_response_form,
            'team_form': team_form,
            'team_add_form': team_add_form,
            'team_files': team_files,
            'per': per
            })

def is_accepted_task_offer_offerer(accepted_task_offer, user):
    return accepted_task_offer and accepted_task_offer.offerer == user.profile

def handle_delivery_request(request, task):
    deliver_form = DeliveryForm(request.POST, request.FILES)
    if deliver_form.is_valid():
        delivery = deliver_form.save(commit=False)
        delivery.task = task
        delivery.delivery_user = user.profile
        delivery.save()
        task.status = "pa"
        task.save()

def handle_delivery_response_request(request, task):
    instance = get_object_or_404(Delivery, id=request.POST.get('delivery-id'))
    deliver_response_form = TaskDeliveryResponseForm(request.POST, instance=instance)
    if deliver_response_form.is_valid():
        delivery = deliver_response_form.save()
        delivery.responding_time = timezone.now()
        delivery.responding_user = user.profile
        delivery.save()

        if delivery.status == 'a':
            task.status = "pp"
            task.save()
        elif delivery.status == 'd':
            task.status = "dd"
            task.save()

def handle_team_request(request, task):
    team_form = TeamForm(request.POST)
    if (team_form.is_valid()):
        team = team_form.save(False)
        team.task = task
        team.save()

def handle_team_add_request(request):
    instance = get_object_or_404(Team, id=request.POST.get('team-id'))
    team_add_form = TeamAddForm(request.POST, instance=instance)
    if team_add_form.is_valid():
        team = team_add_form.save(False)
        team.members.add(*team_add_form.cleaned_data['members'])
        team.save()

def handle_permissions_request(request, task):
    for t in task.teams.all():
        for f in task.files.all():
            try:
                tft_string = 'permission-perobj-' + str(f.id) + '-' + str(t.id)
                tft_id=request.POST.get(tft_string)
                instance = TaskFileTeam.objects.get(id=tft_id)
            except TaskFileTeam.DoesNotExist as e:
                instance = TaskFileTeam(
                    file = f,
                    team = t,
                )

            instance.read = request.POST.get('permission-read-' + str(f.id) + '-' + str(t.id))  or False
            instance.write = request.POST.get('permission-write-' + str(f.id) + '-' + str(t.id)) or False
            instance.modify = request.POST.get('permission-modify-' + str(f.id) + '-' + str(t.id))  or False
            instance.save()
        t.write = request.POST.get('permission-upload-' + str(t.id)) or False
        t.save()

@login_required
def task_permissions(request, project_id, task_id):
    user = request.user
    task = Task.objects.get(pk=task_id)
    project = Project.objects.get(pk=project_id)
    accepted_task_offer = task.accepted_task_offer()

    if project.user == user.profile or user == accepted_task_offer.offerer.user:
        if int(project_id) == task.project.id:
            if handle_task_permission_request(request, task): 
                return redirect('task_view', project_id=project_id, task_id=task_id)
            else:
                return render(
                    request,
                    'projects/task_permissions.html',
                    {
                        'project': project,
                        'task': task,
                        'form': TaskPermissionForm(),
                    }
                )
    return redirect('task_view', project_id=project_id, task_id=task_id)

def handle_task_permission_request(request, task):
    if request.method == 'POST':
        task_permission_form = TaskPermissionForm(request.POST)
        if task_permission_form.is_valid():
            try:
                username = task_permission_form.cleaned_data['user']
                user = User.objects.get(username=username)
                permission_type =task_permission_form.cleaned_data['permission']
                if permission_type == 'Read':
                    task.read.add(user.profile)
                elif permission_type == 'Write':
                    task.write.add(user.profile)
                elif permission_type == 'Modify':
                    task.modify.add(user.profile)
            except User.DoesNotExist:
                print("user not found")
        return True
    return False

@login_required
def delete_file(request, file_id):
    f = TaskFile.objects.get(pk=file_id)
    f.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
