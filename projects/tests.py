from django.test import TestCase, Client
from django.urls import reverse


from unittest import skip

from .models import TaskOffer, Task, Project, Team
from user.models import Profile
from .forms import TaskOfferForm

from .views import get_user_task_permissions, new_project
from projects.models import ProjectCategory

from django.contrib.auth.models import User

from .templatetags import project_extras

# Create your tests here.

class ProjectViewTest(TestCase):

    fixtures = ['test_seed.json']

    def setUp(self):
        self.c = Client()
        response = self.c.post('/user/login/', {'username': 'admin', 'password': 'qwerty123'})
        self.assertEquals(302, response.status_code)
        self.joe = Client()
        offer = self.joe.post('/user/login/', {'username': 'joe', 'password': 'qwerty123'})
        self.assertEquals(302, response.status_code)
        ProjectCategory.objects.create(name='Cleaning')

    def test_offer_response(self):
        offer = TaskOffer.objects.get(pk=1)
        offer.status = 'd'
        offer.save()

        response = self.c.post('/projects/1/', 
            {
                'offer_response' : '',
                'status' : 'a',
                'feedback' : 'This is a great test offer!',
                'taskofferid' : 1,
            })
        self.assertEquals(200, response.status_code)
        
        offer = TaskOffer.objects.get(pk=1)
        
        self.assertEquals('a', offer.status)

    def test_status_change(self):
        proj = Project.objects.get(pk=1)
        self.assertEqual(proj.status, 'o')

        response = self.c.post('/projects/1/', 
            {
                'status_change' : '',
                'status' : 'i',
            })
        self.assertEquals(200, response.status_code)
        
        proj = Project.objects.get(pk=1)
        self.assertEqual(proj.status, 'i')


    def test_offer_submit(self):
        response = self.joe.post('/projects/1/', 
            {
                'offer_submit':'',
                'taskvalue': 1,
                'title': 'test offer',
                'description': 'This is an test offer!',
                'price': 100,
            })
        
        self.assertEquals(200, response.status_code)

        offer = TaskOffer.objects.get(pk=2)
        self.assertEqual(offer.title, 'test offer')


class GetUserTaskPermissionTest(TestCase):
    fixtures = ['test_seed.json']

    def setUp(self):
        self.adm = User.objects.get(pk=1)
        self.joe = User.objects.get(pk=2)
        self.hp = User.objects.get(pk=3)
        self.task = Task.objects.get(pk=1)

    def test_accepted_user(self):
        perms = get_user_task_permissions(self.hp, self.task)

        self.assertTrue(perms.write)
        self.assertTrue(perms.read)
        self.assertTrue(perms.modify)
        self.assertFalse(perms.owner)
        self.assertTrue(perms.upload)

    def test_task_participants_permissions(self):
        perms = get_user_task_permissions(self.joe, self.task)

        self.assertFalse(perms.write)
        self.assertFalse(perms.read)
        self.assertFalse(perms.modify)
        self.assertFalse(perms.owner)
        self.assertFalse(perms.upload)
        self.assertFalse(perms.view_task)

        self.joe.profile.task_participants_read.add(self.task)
        t = Team.objects.create(name="Test Team", task=self.task, write=True)
        self.joe.profile.teams.add(t)

        self.joe.profile.task_participants_write.add(self.task)
        self.joe.profile.task_participants_modify.add(self.task)

        perms = get_user_task_permissions(self.joe, self.task)

        self.assertTrue(perms.write)
        self.assertTrue(perms.read)
        self.assertTrue(perms.modify)
        self.assertFalse(perms.owner)
        self.assertTrue(perms.upload)
        self.assertTrue(perms.view_task)

class ProjectIntegrationAndSystemTest(TestCase):
    fixtures = ["test_seed.json"]

    def setUp(self):
        self.c = Client()
        response = self.c.post('/user/login/', {'username': 'admin', 'password': 'qwerty123'})
        self.assertEquals(302, response.status_code)

    def test_edit_project(self):
        TEST_DESCRIPTION = 'test description 123'
        response = self.c.post('/projects/1/', 
            {
                'edit_project':'',
                'description': TEST_DESCRIPTION,
            })
        self.assertEquals(200, response.status_code)
        proj = Project.objects.get(pk=1)
        self.assertEqual(proj.description, TEST_DESCRIPTION)

    def test_add_task(self):
        response = self.c.post('/projects/1/', 
            {
                'add_task':'',
                'title': 'testtask',
                'description': 'test task description',
                'budget': 400
            })
        
        self.assertEquals(200, response.status_code)

        proj = Project.objects.get(pk=1)
        task = proj.tasks.filter(title='testtask')
        self.assertEqual(len(task), 1)
        task = task[0]

        self.assertEqual(task.description, 'test task description')
        self.assertEqual(task.budget, 400)

DEFAULT_PROJECT_OFFER_DATA = {
    'title': 'a title', 
    'description': 'a description', 
    'price': 450
    }

class ProjectOffersTest(TestCase):
    fixtures = ["test_seed.json"]

    def setUp(self):
        self.c = Client()
        response = self.c.post('/user/login/', {'username': 'admin', 'password': 'qwerty123'})
        self.assertEquals(302, response.status_code)
        self.harrypotter = Client()
        offer = self.harrypotter.post('/user/login/', {'username': 'harrypotter', 'password': 'qwerty123'})
        self.assertEquals(302, response.status_code)

    def test_give_project_offer(self):
        form_data = DEFAULT_PROJECT_OFFER_DATA.copy()
        form = TaskOfferForm(data=form_data)
        self.assertTrue(form.is_valid())

    @skip('An offer should not be valid when price is negative.')
    def test_give_project_offer_negative_price(self):
        form_data = DEFAULT_PROJECT_OFFER_DATA.copy()
        form_data['price'] = -1
        form = TaskOfferForm(data=form_data)
        self.assertFalse(form.is_valid())
    
    def test_give_project_offer_empty_price(self):
        form_data = DEFAULT_PROJECT_OFFER_DATA.copy()
        form_data['price'] = ""
        form = TaskOfferForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_give_project_offer_empty_title(self):
        form_data = DEFAULT_PROJECT_OFFER_DATA.copy()
        form_data['title'] = ""
        form = TaskOfferForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_give_project_offer_empty_description(self):
        form_data = DEFAULT_PROJECT_OFFER_DATA.copy()
        form_data['description'] = ""
        form = TaskOfferForm(data=form_data)
        self.assertFalse(form.is_valid())

class AcceptingOffers(TestCase):
    fixtures = ["test_seed.json"]
    
    def setUp(self):
        self.c = Client()
        response = self.c.post('/user/login/', {'username': 'admin', 'password': 'qwerty123'})
        self.assertEquals(302, response.status_code)
        self.joe = Client()
        offer = self.joe.post('/user/login/', {'username': 'joe', 'password': 'qwerty123'})
        self.assertEquals(302, response.status_code)
    
    def test_offer_response_declined(self):
        offer = TaskOffer.objects.get(pk=1)
        offer.status = 'p'
        offer.save()

        response = self.c.post('/projects/1/', 
            {
                'offer_response' : '',
                'status' : 'd',
                'feedback' : 'This is a great test offer!',
                'taskofferid' : 1,
            })
        self.assertEquals(200, response.status_code)
        
        offer = TaskOffer.objects.get(pk=1)
        
        self.assertEquals('d', offer.status)

    def test_offer_response_pending(self):
        offer = TaskOffer.objects.get(pk=1)
        offer.status = 'p'
        offer.save()

        response = self.c.post('/projects/1/', 
            {
                'offer_response' : '',
                'status' : 'p',
                'feedback' : 'This is a great test offer!',
                'taskofferid' : 1,
            })
        self.assertEqual(200, response.status_code)

        offer = TaskOffer.objects.get(pk=1)
        
        self.assertEquals('p', offer.status)

    def test_offer_response_accepted(self):
        offer = TaskOffer.objects.get(pk=1)
        offer.status = 'p'
        offer.save()

        response = self.c.post('/projects/1/', 
            {
                'offer_response' : '',
                'status' : 'a',
                'feedback' : 'This is a great test offer!',
                'taskofferid' : 1,
            })
        self.assertEqual(200, response.status_code)

        offer = TaskOffer.objects.get(pk=1)
        
        self.assertEquals('a', offer.status)

    def test_offer_response_invalid_status_should_not_change_status(self):
        offer = TaskOffer.objects.get(pk=1)
        offer.status = 'p'
        offer.save()

        response = self.c.post('/projects/1/', 
            {
                'offer_response' : '',
                'status' : 'x',
                'feedback' : 'This is a great test offer!',
                'taskofferid' : 1,
            })
        self.assertEqual(200, response.status_code)

        offer = TaskOffer.objects.get(pk=1)
        
        self.assertEquals('p', offer.status)
    

class ProjectOffersAcceptingTest(TestCase):
    fixtures = ["test_seed.json"]

    def setUp(self):
        self.c = Client()
        response = self.c.post('/user/login/', {'username': 'admin', 'password': 'qwerty123'})
        self.assertEquals(302, response.status_code)
        self.harrypotter = Client()
        offer = self.harrypotter.post('/user/login/', {'username': 'harrypotter', 'password': 'qwerty123'})
        self.assertEquals(302, response.status_code)

        proj = Project.objects.create(
            category_id = 2,
            user_id = 1
        )

        self.task = Task.objects.create(
            title = "test task 1",
            description = "test task description 1",
            budget = 100,
            project = proj)

    def test_accept_non_existing_task_except_raise(self):
        task_offer = Task.accepted_task_offer(self.task)
        self.assertIsNone(task_offer)

    def test_accept_existing_task(self):
        task = Task.objects.get(pk=1)
        expecting_offer = TaskOffer.objects.get(pk=1)
        task_offer = Task.accepted_task_offer(task)
        self.assertEquals(expecting_offer, task_offer)

class NewProjectTest(TestCase):

    fixtures = ['test_seed.json']

    def setUp(self):
        self.c = Client()
        response = self.c.post('/user/login/', {'username': 'admin', 'password': 'qwerty123'})
        self.assertEquals(302, response.status_code)
        self.assertEquals('/', response.url)
        ProjectCategory.objects.create(name='Cleaning')

    def test_new_project_request(self):
        response = self.c.post('/projects/new/', 
            {
                'title' : 'test project',
                'description' : 'test description',
                'category_id' : 1,
            })
        self.assertEquals(302, response.status_code)
        self.assertEquals('/projects/2/', response.url)

        self.assertEquals(Project.objects.get(pk=2).title, 'test project')
        
